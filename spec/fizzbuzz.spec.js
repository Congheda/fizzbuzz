const FizzBuzz = require('../src/fizzbuzz.js') 

describe('Fizzbuzz', () => {
    it('Retorna Fizz si es divisible entre 3', () => {
        // Arrange - Organizar
        const expected_value = "Fizz"
        const fizzBuzz = new FizzBuzz()

        // Act
        const result = fizzBuzz.getString(6)

        // Assert - comprobar
        expect(result).toEqual(expected_value)
    })
    it('Retorna Buzz si es divisible entre 5', () => {
        // Arrange - Organizar
        const expected_value = "Buzz"
        const fizzBuzz = new FizzBuzz()

        // Act
        const result = fizzBuzz.getString(10)

        // Assert - comprobar
        expect(result).toEqual(expected_value)
    })
    it('Retorna FizzBuzz si es divisible entre 5 y 3', () => {
        // Arrange - Organizar
        const expected_value = "FizzBuzz"
        const fizzBuzz = new FizzBuzz()

        // Act
        const result = fizzBuzz.getString(15)

        // Assert - comprobar
        expect(result).toEqual(expected_value)
    })
    it ('Retorna el numero si no es divisible ni 3 ni 5', () => {
        //Organizar
        const expect_value = "4"
        const fizzBuzz = new FizzBuzz()

        //ACt
        const result = fizzBuzz.getString(4)

        //Comprobar
        expect(result).toEqual(expect_value)

    })
    it ('Devuelve Fizz si contiene el 3', () => {
        //Organizar
        const expect_value = "Fizz"
        const fizzBuzz = new FizzBuzz()

        //Act
        const result = fizzBuzz.getString(13)

        //Comprobar
        expect(result).toEqual(expect_value)
    })
})
