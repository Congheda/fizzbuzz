FROM node:14.15.4

RUN apt-get update

RUN npm install

WORKDIR /app

COPY package.json package-lock.json /app/

RUN npm install

COPY . /app

CMD ["npm", "run", "start"]